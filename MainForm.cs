﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace ScottPalletConfig
{
    public enum PalletPattern
    {
        p4U1,
        p6U1
    }
    
    public partial class MainForm : Form
    {
        double palletLen = 0.0;
        double palletWid = 0.0;
        double productLen = 0.0;
        double productWid = 0.0;
        PalletPattern pattern;
        Point pallet_centre = new Point();
        Point place_origin = new Point();
        bool updatePanel1 = false;
        bool updatePanel2 = false;
        int scalingFactor = 1;

        double[,] boxOriginsNormal4U1 = new double[,]{
                                    {0,0,0,0},
                                    {0,1,0,0},
                                    {0,0,1,0},
                                    {0,1,1,0}
                                    };

        double[,] boxCentresNormal4U1 = new double[,]{
                                    {0,0.5,0.5,0},
                                    {0,1.5,0.5,0},
                                    {0,0.5,1.5,0},
                                    {0,1.5,1.5,0}
                                    };






        public MainForm()
        {
            InitializeComponent();
            
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void showLayoutBtn_Click(object sender, EventArgs e)
        {
           
            try
            {
                palletLen = double.Parse(palletLengthTB.Text);
                palletWid = double.Parse(palletWidthTB.Text);
                productLen = double.Parse(productLengthTB.Text);
                productWid = double.Parse(productWidthTB.Text);
                pattern = (PalletPattern)patternCB.SelectedIndex;
            }
            catch (Exception)
            {
                MessageBox.Show("Values must be real numbers!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            generateBoxesNormalLayer4U1();
            drawPallet(tabPage2);
            drawPallet(tabPage1);
           
         



        }


        private void drawPallet(TabPage tabPage)
        {
            if (tabPage==tabPage1)
            {
                updatePanel1 = true;
                updatePanel2 = false;
            }
            else
            {
                updatePanel1 = false;
                updatePanel2 = true;
            }
            displayPanel1.Invalidate();



        }

        private void generateBoxesNormalLayer4U1()
        {

            StringBuilder offsets = new StringBuilder();
            int x_offset = 0;
            int y_offset = 0;
            int z_offset = 35;
            string fn = @"C:\Users\lparsons\Documents\scottpalletconfigureapp\Offsets_4U1.txt";

            pallet_centre.X = (int)palletLen / 2;
            pallet_centre.Y = (int)palletWid / 2;

            place_origin.X = (int)(palletLen - 2 * productLen) / 2;
            place_origin.Y = (int)(palletWid - 2 * productWid) / 2;
            
            //generate place position data
            for (int i = 0; i < 4; i++)
            {
                x_offset = (int)(place_origin.X + boxCentresNormal4U1[i, 0] * productWid + boxCentresNormal4U1[i, 1] * productLen);
                y_offset = (int)(place_origin.Y + boxCentresNormal4U1[i, 2] * productWid + boxCentresNormal4U1[i, 3] * productLen);
                offsets.Append($"[{x_offset},{y_offset},{z_offset}]\n");
            }

            using(StreamWriter w = new StreamWriter(fn))
            {
                w.Write(offsets);
            }

            

        }

        

      

        private void displayPanel1_Paint(object sender, PaintEventArgs e)
        {
            Pen palletPen = new Pen(Color.Brown, 2);
            palletPen.Alignment = PenAlignment.Center;
            
            
            if (updatePanel1)
            {
                //Pallet
                e.Graphics.DrawRectangle(palletPen, new Rectangle(
                    0,
                    0,
                    (int)palletLen / scalingFactor,
                    (int)palletWid / scalingFactor));

                for (int i = 0; i < 4; i++)
                {
                    //Boxes test
                    e.Graphics.DrawRectangle(Pens.Blue, new Rectangle(
                        (int)(place_origin.X  + boxOriginsNormal4U1[i, 0] * productWid + boxOriginsNormal4U1[i, 1] * productLen),
                        (int)(place_origin.Y  + boxOriginsNormal4U1[i, 2] * productWid + boxOriginsNormal4U1[i, 3] * productLen),
                        (int)productLen / scalingFactor,
                        (int)productWid / scalingFactor));


                    //Box Centres
                    
                    e.Graphics.DrawRectangle(Pens.SteelBlue, new Rectangle(
                        (int)(place_origin.X + boxCentresNormal4U1[i, 0] * productWid + boxCentresNormal4U1[i, 1] * productLen),
                        (int)(place_origin.Y + boxCentresNormal4U1[i, 2] * productWid + boxCentresNormal4U1[i, 3] * productLen),
                        1,
                        1));
                   
                }


            }
        }
    }
}
